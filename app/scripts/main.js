(function($){
	'use strict';

	// Carousel
	$('.carousel').carousel();

	//Sidr Navigation
	$('.mobile_nav_trigger').sidr({
		side: 'right'
	});

	$('a.mobile_nav_trigger').click(function(e){
		e.preventDefault();
	});

	// Parallax
	$(document).ready(function(){
		// Cache the Window object
		var $window = $(window);

		$('section[data-type="background"]').each(function(){
			var $bgobj = $(this); // assigning the object

			$(window).scroll(function() {

				// Scroll the background at var speed
				// the yPos is a negative value because we're scrolling it UP!								
				var yPos = -($window.scrollTop() / $bgobj.data('speed'));

				// Put together our final background position
				var coords = '50% '+ yPos + 'px';

				// Move the background
				$bgobj.css({ backgroundPosition: coords });

			}); // window scroll Ends

		});
	});

	// FitVids 
	$('.testimonials-videos').fitVids();

	// Sicky Nav
	var el = $( '#banner' ),
        elPosition = el.offset(),
        body = $( 'body' );
            
    if( elPosition !== undefined ){
    
        $(window).on('scroll', function() {

            if ( $(window).width() > 767 ){
            
                if( $(this).scrollTop() > (elPosition.top + 125) ){

					el.addClass( 'is-sticky' );
					if( body.hasClass('l-internal') ) {
						body.css('paddingTop', el.height()  );
					}
				
                } else{
                    el.removeClass( 'is-sticky' );
                    if( body.hasClass('l-internal') ) {
						body.css('paddingTop', 0 );
                   	}
                }
                
            } else{
                return false;
            }
        });
    }

    // Smooth Scroll
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
				scrollTop: target.offset().top - 135
				}, 1000);
				return false;
			}
		}
	});


	var testimonialTrigger = 0,
		testimonialsSection = $('.homepage .testimonials'),
		testimonials = $('.homepage .testimonials .testimonial');

	console.log(testimonialTrigger);

	function animateSettings() {
		testimonials.animate({
			'opacity': 1,
			'position': 'relative',
			'top': 0
		}, 800);
	}

	function testimonialSettings() {
		testimonials.css({
			'opacity': 0,
			'position': 'relative',
			'top': -100
		});
	}

	if( testimonialTrigger === 0 ) {
		testimonialSettings();
	}
	

	$(window).on('scroll', function() {
		//console.log(testimonialTrigger);
		if ( ($(this).scrollTop() >= ( testimonialsSection.offset().top - 120)) &&  testimonialTrigger === 0  ) {
			testimonialTrigger = 1;

			if( testimonialTrigger === 1 ) {
				animateSettings();
			}
		}
	});


})(jQuery);